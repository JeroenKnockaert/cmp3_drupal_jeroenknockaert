	<?php print render($page['header']); ?>
	<div class="wrapper">
		<div class="container">
			<div id="logo">
				<h1><a href="<?php print url('<front>', array('absolute' => TRUE)); ?>" title="Back to the homepage">Jeroen Knockaert</a></h1>
			</div> <!-- end id logo -->
			<nav class="main clearfix" id="navigation">
				<div id="dl-menu" class="dl-menuwrapper">
				<button class="dl-trigger">Open Menu</button>
								<?php if ($main_menu): ?>

									<?php print theme('links__system_main_menu', array(
									'links' => $main_menu,
									'attributes' => array(
										'id' => 'menu-navigation',
										'class' => array('dl-menu'),

									),
									)); ?>
					
								<?php endif; ?>
				</div>
			</nav>
		</div> <!-- end class container -->
	</div> <!-- end class wrapper -->
	
	<!-- THIS IS THE END OF THE HEADER -->
	<div class="container">

		<div class="col col-12 work">
			<div class="broodkruimels">
				<a href="#" class="prev-page">home</a>
				<p class="slash">//</p>
				<a href="" class="active-page"><?php echo $title; ?></a>
			</div> <!-- end class broodkruimels -->
			<h2><?php echo $title; ?></h2>
	
			<!-- FETCH ALL POSTS -->
						
				<div class="single-post" id="post-getal">				
					<!-- CHECK IF POST HAS IMAGE FIRST -->
					<a href ="#" class="single-post-image">
						<img width="348" height="122" src="http://fpoimg.com/348x122" class="attachment-custom-homepage-image wp-post-image" alt="titel van post">
					</a>
					<!-- END IF WILL BE HERE -->		
				</div> <!-- end single-post -->
				
				<div class="single-post" id="post-getal">				
					<!-- CHECK IF POST HAS IMAGE FIRST -->
					<a href ="#" class="single-post-image">
						<img width="348" height="122" src="http://fpoimg.com/348x122" class="attachment-custom-homepage-image wp-post-image" alt="titel van post">
					</a>
					<!-- END IF WILL BE HERE -->		
				</div> <!-- end single-post -->	
				
				<div class="single-post" id="post-getal">				
					<!-- CHECK IF POST HAS IMAGE FIRST -->
					<a href ="#" class="single-post-image">
						<img width="348" height="122" src="http://fpoimg.com/348x122" class="attachment-custom-homepage-image wp-post-image" alt="titel van post">
					</a>
					<!-- END IF WILL BE HERE -->		
				</div> <!-- end single-post -->
				
				<div class="single-post" id="post-getal">				
					<!-- CHECK IF POST HAS IMAGE FIRST -->
					<a href ="#" class="single-post-image">
						<img width="348" height="122" src="http://fpoimg.com/348x122" class="attachment-custom-homepage-image wp-post-image" alt="titel van post">
					</a>
					<!-- END IF WILL BE HERE -->		
				</div> <!-- end single-post -->
							
			<!-- END FETCH POSTS -->

		</div> <!-- end class col12 -->
	</div> <!-- end class container -->
	
		<!-- FOOTER -->
	<footer>
		<p>&copy 2015 <a href="#">Jeroen Knockaert</a>. All rights reserved.</p>
	</footer>
