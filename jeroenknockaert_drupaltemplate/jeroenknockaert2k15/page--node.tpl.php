<?php
$formatted_date = format_date($node->created, 'custom', 'M j, Y');
?>
	
	<?php print render($page['header']); ?>
	<div class="wrapper">
		<div class="container">
			<div id="logo">
				<h1><a href="<?php print url('<front>', array('absolute' => TRUE)); ?>" title="Back to the homepage">Jeroen Knockaert</a></h1>
			</div> <!-- end id logo -->
			<nav class="main clearfix" id="navigation">
				<div id="dl-menu" class="dl-menuwrapper">
				<button class="dl-trigger">Open Menu</button>
								<?php if ($main_menu): ?>

									<?php print theme('links__system_main_menu', array(
									'links' => $main_menu,
									'attributes' => array(
										'id' => 'menu-navigation',
										'class' => array('dl-menu'),

									),
									)); ?>
					
								<?php endif; ?>
				</div>
			</nav>
		</div> <!-- end class container -->
	</div> <!-- end class wrapper -->
	
	<!-- THIS IS THE END OF THE HEADER -->
<div class="container">
		<div class="col col-8">	
			<div class="broodkruimels">
				<p class="slash"><?=$formatted_date?></p>

			</div> <!-- end class broodkruimels -->

			<h1><?php echo $title; ?></h1>
			
			<!-- check if post has an image -->

			<!-- DISPLAY ALL CATEGORIES OF POST -->
			<a href="#" class="category" title="title van category">Category naam</a>

			<div class="post-info">

				<?php print render($page['content']); ?>
			</div>
						
		</div> <!-- end class col-8 -->
			<div class="container">
		<div class="col-4">
			<div class="sidebar-widget">
				<div class="newsletter">
					<div class="es_caption">Want to recieve an email when I post new work? Subscribe below for my newsfeed.</div>
					<div class="es_msg"><span id="es_msg"></span></div>
					<div class="es_lablebox">Email</div>
					<div>
						<label for="es_txt_email">Email</label>
						<input class="es_textbox_class" name="es_txt_email" id="es_txt_email" onkeypress="if(event.keyCode==13) es_submit_page('http://jeroenknockaert.be')" value="" maxlength="225" type="email" placeholder="enter your email name here">
					</div>
					<div class="es_lablebox">Name</div>
					<div class="es_textbox">
						<label for="es_txt_name">Name</label>
						<input class="es_textbox_class" name="es_txt_name" placeholder="enter your name here" id="es_txt_name" value="" maxlength="225" type="text">
					</div>
					<input name="es_txt_group" id="es_txt_group" value="" type="hidden"><div class="es_button">
					<input class="es_textbox_button" name="es_txt_button" id="es_txt_button" onclick="return es_submit_page('http://jeroenknockaert.be')" value="Subscribe" type="button"></div>
				</div>
			</div>
			<div class="sidebar-widget">
				<div class="col col-4 more-work">
					<div class="more-work-title"><h4>More work</h4></div>
					
					<!-- HERE COMES A LOOP FOR EACH POST -->

					<?php if ($page["more_work"]): ?>
						<?php print render($page['more_work']); ?>
					<?php endif; ?>


				</div>
			</div>
		</div>
	</div>
 </div>
 
 	<!-- FOOTER -->
	<footer>
		<p>&copy 2015 <a href="#">Jeroen Knockaert</a>. All rights reserved.</p>
	</footer>
	
	<!-- BOTTOM SCRIPTS -->
	<!-- JQUERY CDN FIRST OTHERWISE LOCAL FALLBACK -->
	<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="components/jquery/jquery-2.1.0.min.js">\x3C/script>')</script>
	
	<!-- MODERNIZR: NEW HTML5 ELEMENTS + FEATURE DETECTION -->
	<script type="text/javascript" src="./components/modernizr/modernizr.custom.js"></script>
	
	<!-- Latest compiled and minified Bootstrap JavaScript -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	
	<!-- CUSTOM JAVASCRIPT FILES -->
	<script type="text/javascript" src="./js/global.js"></script>
	<script type="text/javascript" src="./js/google.js"></script>

	<!-- CODE VOOR DE SLIDER -->
	<script type="text/javascript" src="./js/jquery.cycle.all.js"></script>	
	<script type="text/javascript" src="./js/jquery.dlmenu.js"></script>
	
	<script>
		$(function() {
			$( '#dl-menu' ).dlmenu();
		});
	</script>