	<!-- ====================================================
	header section -->
	
			
	
	<?php print render($page['header']); ?>
	<div class="wrapper">
		<div class="container">
			<div id="logo">
				<h1><a href="<?php print url('<front>', array('absolute' => TRUE)); ?>" title="Back to the homepage">Jeroen Knockaert</a></h1>
			</div> <!-- end id logo -->
			<nav class="main clearfix" id="navigation">
				<div id="dl-menu" class="dl-menuwrapper">
				<button class="dl-trigger">Open Menu</button>
								<?php if ($main_menu): ?>

									<?php print theme('links__system_main_menu', array(
									'links' => $main_menu,
									'attributes' => array(
										'id' => 'menu-navigation',
										'class' => array('dl-menu'),

									),
									)); ?>
					
								<?php endif; ?>
				</div>
			</nav>
		</div> <!-- end class container -->
	</div> <!-- end class wrapper -->
	
	<!-- THIS IS THE END OF THE HEADER -->

	<!-- START OF 9 GRID WITH DIFF WORK -->
	<div class="container">
		
		<div class="col col-12 work">
			<div class="broodkruimels">
				<a href="<?php print url('<front>', array('absolute' => TRUE)); ?>" class="prev-page">home</a>
				<p class="slash">//</p>
				<a href="" class="active-page"><?php echo $title; ?> </a>
			</div> <!-- end class broodkruimels -->
			<h2><?php echo $title; ?></h2>

					<?php //print render($content['field_afbeelding2']); ?>
				
					<?php print render($page['content']); ?>
			<!-- LOOP FOR POSTS WILL START HERE -->
					
				<a href="#" title="titel van de post" class="single-post" id="post-id">
					<!-- CHECK IF POST HAS IMAGE FIRST -->	
					<div class="single-post-image">
						<img width="348" height="122" src="http://fpoimg.com/348x122" class="attachment-custom-homepage-image wp-post-image" alt="title van de post">
					</div>
					<!-- END IF WILL BE HERE -->		
				</a> <!-- end work-post -->

				<a href="#" title="titel van de post" class="single-post" id="post-id">
					<!-- CHECK IF POST HAS IMAGE FIRST -->	
					<div class="single-post-image">
						<img width="348" height="122" src="http://fpoimg.com/348x122" class="attachment-custom-homepage-image wp-post-image" alt="title van de post">
					</div>
					<!-- END IF WILL BE HERE -->		
				</a> <!-- end work-post -->
				
				<a href="#" title="titel van de post" class="single-post" id="post-id">
					<!-- CHECK IF POST HAS IMAGE FIRST -->	
					<div class="single-post-image">
						<img width="348" height="122" src="http://fpoimg.com/348x122" class="attachment-custom-homepage-image wp-post-image" alt="title van de post">
					</div>
					<!-- END IF WILL BE HERE -->		
				</a> <!-- end work-post -->
				
			<!-- THE LOOP WILL END HERE -->

		</div> <!-- end class col12 -->

	</div> <!-- end class container -->
	
	<!-- FOOTER -->
	<footer>
		<p>&copy 2015 <a href="#">Jeroen Knockaert</a>. All rights reserved.</p>
	</footer>