//NAMELESS FUNCTION - AUTO EXECUTED WHEN DOCUMENT IS LOADED
(function(){

    //SCROLL TO TOP
    $('.scrolltotop').click(function(ev){
        ev.preventDefault();
        $('html, body').animate({scrollTop:0},600);
        return false;
    });

	
	//VERANDER TABLE ROW PER RIJ EEN ANDER KLEUR
	$(document).ready(function() {
		$("table.tracklist tr").filter(":odd").addClass("paint");
	});
	
	// hover per rij
	$("tr").not(':first').hover(
	  function () {
		$(this).css("background","#1f1f1f");
	  }, 
	  function () {
		$(this).css("background","");
	  }
	);

	/*
	$(document).ready(function() {
		$("table.tracklist tr").filter(":odd").css("background-color","red");
	});
	*/
    //FADE IN AND OUT SCROLLTOTOP
    $(window).scroll(function(ev){
        if($(this).scrollTop() > 60)
            $('.scrolltotop').fadeIn();
        else
            $('.scrolltotop').fadeOut();
    });

	$(document).ready(function(){

        $(".show_hide").show();

    $('.show_hide').click(function()
	{
		$(this).slideToggle();

			$(this).children('.video-info').fadeOut('slow', function()
			{
				$(this).parent().parent().children(".slidingDiv").children(".thevideo").css("display","block");

			});
		});

	});
	
	$(document).ready(function() {
	$('#slider').cycle({ 
		fx:      'fade', 
		fx:   'scrollHorz', 
		next:	'#next',
		prev:	'#prev'
		 
	});
});

		$(function() {
			$( '#dl-menu' ).dlmenu();
		});
		
})();