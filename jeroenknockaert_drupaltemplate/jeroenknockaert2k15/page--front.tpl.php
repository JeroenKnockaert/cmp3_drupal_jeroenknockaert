	<!-- ====================================================
	header section -->
	
			
	
	<?php print render($page['header']); ?>
	<div class="wrapper">
		<div class="container">
			<div id="logo">
				<h1><a href="<?php print url('<front>', array('absolute' => TRUE)); ?>" title="Back to the homepage">Jeroen Knockaert</a></h1>
			</div> <!-- end id logo -->
			<nav class="main clearfix" id="navigation">
				<div id="dl-menu" class="dl-menuwrapper">
				<button class="dl-trigger">Open Menu</button>
				<?php if ($main_menu): ?>

					<?php print theme('links__system_main_menu', array(
					'links' => $main_menu,
					'attributes' => array(
						'id' => 'menu-navigation',
						'class' => array('dl-menu'),

					),
					)); ?>
					
					<?php endif; ?>
				</div>
			</nav>
		</div> <!-- end class container -->
	</div> <!-- end class wrapper -->
	
	<!-- THIS IS THE END OF THE HEADER -->
	
	<!-- START CONTENT STARTS HERE -->
	
  
	<!-- START RESPONSIVE SLIDER STARTS HERE -->
	<div class="container">
		<div class="slideshow">
			<div id="prev"><i class="fa fa-chevron-left"></i></div>
				<div id="slider">
					
					<!-- LOOP FOR POSTS WILL START HERE -->
					
						<a href="#" title="title van de post" class="slideshow-post" id="post-getal">
							<!-- CHECK IF POST HAS IMAGE FIRST -->				
							<div class="slideshow-post-image">
                <?php print render($content['field_afbeelding2']); ?> 
							</div>	
							<!-- END IF WILL BE HERE -->			
						</a> <!-- end work-post -->
						
						<a href="#" title="title van de post" class="slideshow-post" id="post-getal">
							<!-- CHECK IF POST HAS IMAGE FIRST -->				
							<div class="slideshow-post-image">
								<img width="1140" height="400" src="http://fpoimg.com/1140x400" class="attachment-post-thumbnail wp-post-image" alt="title van de post">
							</div>	
							<!-- END IF WILL BE HERE -->			
						</a> <!-- end work-post -->
						
					<!-- THE LOOP WILL END HERE -->
					
				</div> <!-- end class slider -->	
			<div id="next"><i class="fa fa-chevron-right"></i></div>
		</div> <!-- end class slideshow -->
	</div> <!-- end class container -->
	<!-- END OF RESPONSIVE SLIDER IS HERE -->
	
	<!-- START OF 9 GRID WITH DIFF WORK -->
	<div class="container">
		<div class="col col-12">
			<h2>work</h2>
						<?php //hide($content['title']); ?>
						
						<?php print render($page['content']); ?>
						
			<!-- LOOP FOR POSTS WILL START HERE -->

				<a href="#" title="titel van de post" class="single-post" id="post-id">
					<!-- CHECK IF POST HAS IMAGE FIRST -->	
					<div class="single-post-image">
						<img width="348" height="122" src="http://fpoimg.com/348x122" class="attachment-custom-homepage-image wp-post-image" alt="title van de post">
					</div>
					<!-- END IF WILL BE HERE -->		
				</a> <!-- end work-post -->

			<!-- THE LOOP WILL END HERE -->

		</div> <!-- end class col12 -->
	</div> <!-- end class container -->
	
	<!-- FOOTER -->
	<footer>
		<p>&copy 2015 <a href="#">Jeroen Knockaert</a>. All rights reserved.</p>
	</footer>
