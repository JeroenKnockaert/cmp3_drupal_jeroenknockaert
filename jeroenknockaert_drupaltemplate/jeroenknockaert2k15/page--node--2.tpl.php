	<?php print render($page['header']); ?>
	<div class="wrapper">
		<div class="container">
			<div id="logo">
				<h1><a href="<?php print url('<front>', array('absolute' => TRUE)); ?>" title="Back to the homepage">Jeroen Knockaert</a></h1>
			</div> <!-- end id logo -->
			<nav class="main clearfix" id="navigation">
				<div id="dl-menu" class="dl-menuwrapper">
				<button class="dl-trigger">Open Menu</button>
								<?php if ($main_menu): ?>

									<?php print theme('links__system_main_menu', array(
									'links' => $main_menu,
									'attributes' => array(
										'id' => 'menu-navigation',
										'class' => array('dl-menu'),

									),
									)); ?>
					
								<?php endif; ?>
				</div>
			</nav>
		</div> <!-- end class container -->
	</div> <!-- end class wrapper -->
	
	<!-- THIS IS THE END OF THE HEADER -->

		<div class="container">
			
		<div class="col col-12 about">
			<div class="col col-6">
				<p class="intro">Hi, my name is</p>
				<h1>Jeroen Knockaert<span>.</span></h1>

				<div class="about-me">
				I’m a graphic designer living in Belgium. I aspire to make cool and creative designs, regardless of what medium I am working in. I also make websites and videos.
				</div> <!-- end about-me -->

				<hr>
				<h2>6 facts about me</h2>

				<ul>
					<li>I do not have a driver's license.</li>
					<li>Aside from design, my other passion is bowling.</li>
					<li>Music is my life.</li>
					<li>To this day, I would still rather buy music on CDs than buy an mp3.</li>
					<li>If something is not perfect, it is not done.</li>
					<li>My love for steak will never allow me to become a vegetarian.</li>
				</ul>
				<hr>
				<h2>My skills</h2>

			<div class="progress-bars">
				<p style="width:90%" data-value="90">Photoshop</p>
					<progress max="100" value="90" class="html5">
						<!-- Browsers that support HTML5 progress element will ignore the html inside `progress` element. Whereas older browsers will ignore the `progress` element and instead render the html inside it. -->
						<div class="progress-bar">
							<span style="width: 90%">90%</span>
						</div>
					</progress>
					
					<!-- CSS3 -->
				<p style="width:80%" data-value="80">After Effects</p>
					<progress max="100" value="80" class="css3">
						<!-- Browsers that support HTML5 progress element will ignore the html inside `progress` element. Whereas older browsers will ignore the `progress` element and instead render the html inside it. -->
						<div class="progress-bar">
							<span style="width: 80%">80%</span>
						</div>
					</progress>
					
					<!-- jQuery -->
				<p style="width:60%" data-value="60">Illustrator</p>
					<progress max="100" value="60" class="jquery">
						<!-- Browsers that support HTML5 progress element will ignore the html inside `progress` element. Whereas older browsers will ignore the `progress` element and instead render the html inside it. -->
						<div class="progress-bar">
							<span style="width: 60%">60%</span>
						</div>
					</progress>
					
				<!-- Python -->
				<p style="width:90%" data-value="90">Html & Css</p>
					<progress max="100" value="90" class="python">
						<!-- Browsers that support HTML5 progress element will ignore the html inside `progress` element. Whereas older browsers will ignore the `progress` element and instead render the html inside it. -->
						<div class="progress-bar">
							<span style="width: 90%">90%</span>
						</div>
					</progress>
					
					<!-- PHP -->
				<p style="width:65%" data-value="65">Javascript (Jquery)</p>
					<progress max="100" value="65" class="php">
						<!-- Browsers that support HTML5 progress element will ignore the html inside `progress` element. Whereas older browsers will ignore the `progress` element and instead render the html inside it. -->
						<div class="progress-bar">
							<span style="width: 65%">65%</span>
						</div>
					</progress>
					
					<!-- Node.js -->
				<p style="width:80%" data-value="80">PHP</p>
					<progress max="100" value="80" class="node-js">
						<!-- Browsers that support HTML5 progress element will ignore the html inside `progress` element. Whereas older browsers will ignore the `progress` element and instead render the html inside it. -->
						<div class="progress-bar">
							<span style="width: 80%">80%</span>
						</div>
					</progress>     
				
				<p style="width:85%" data-value="85">Wordpress</p>
					<progress max="100" value="85" class="html5">
						<!-- Browsers that support HTML5 progress element will ignore the html inside `progress` element. Whereas older browsers will ignore the `progress` element and instead render the html inside it. -->
						<div class="progress-bar">
							<span style="width: 85%">85%</span>
						</div>
					</progress>
					
					<!-- CSS3 -->
				<p style="width:70%" data-value="70">Laravel</p>
					<progress max="100" value="70" class="css3">
						<!-- Browsers that support HTML5 progress element will ignore the html inside `progress` element. Whereas older browsers will ignore the `progress` element and instead render the html inside it. -->
						<div class="progress-bar">
							<span style="width: 70%">70%</span>
						</div>
					</progress>
				</div><!-- end progress-bars -->
			</div>


		</div> <!-- end content  -->
		<div class="col col-12 about2">
			<div class="col col-6">
			<h1>become my bff</h1>
			<ul class="socialmedia">
				<li><a href="https://www.pinterest.com/Jeroenknockaert" target="_blank"><i class="fa fa-pinterest-square"></i></a></li>
				<li><a href="https://www.facebook.com/Jeroenknockaert" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
				<li><a href="https://www.twitter.com/Jeroenknockaert" target="_blank"><i class="fa fa-twitter-square"></i></a></li>
				<li><a href="https://www.vimeo.com/Jeroenknockaert" target="_blank"><i class="fa fa-vimeo-square"></i></a></li>
				<li><a href="https://www.last.fm/user/Jeroenknockaert" target="_blank"><i class="fa fa-lastfm-square"></i></a></li>
			</ul>
			</div>
			<div class="col col-6 download">
				<h1>Download my cv</h1>
				<p>
					<a href="http://jeroenknockaert.be/wp-content/uploads/2015/09/CV-2015-website.pdf">
					<i class="fa fa-file-pdf-o"></i>click here to download</a>
				</p>
			</div>
		</div>
	</div> <!-- end container -->
	
		<!-- FOOTER -->
	<footer>
		<p>&copy 2015 <a href="#">Jeroen Knockaert</a>. All rights reserved.</p>
	</footer>
	
	<!-- BOTTOM SCRIPTS -->
	<!-- JQUERY CDN FIRST OTHERWISE LOCAL FALLBACK -->
	<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="components/jquery/jquery-2.1.0.min.js">\x3C/script>')</script>
	
	<!-- MODERNIZR: NEW HTML5 ELEMENTS + FEATURE DETECTION -->
	<script type="text/javascript" src="./components/modernizr/modernizr.custom.js"></script>
	
	<!-- Latest compiled and minified Bootstrap JavaScript -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	
	<!-- CUSTOM JAVASCRIPT FILES -->
	<script type="text/javascript" src="./js/global.js"></script>
	<script type="text/javascript" src="./js/google.js"></script>

	<!-- CODE VOOR DE SLIDER -->
	<script type="text/javascript" src="./js/jquery.cycle.all.js"></script>	
	<script type="text/javascript" src="./js/jquery.dlmenu.js"></script>
	
	<script>
		$(function() {
			$( '#dl-menu' ).dlmenu();
		});
	</script>