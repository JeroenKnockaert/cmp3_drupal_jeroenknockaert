//NAMELESS FUNCTION - AUTO EXECUTED WHEN DOCUMENT IS LOADED
(function(){


	$(document).ready(function() {
		$("#es_txt_email").focus(function() {
			$("#es_txt_name").animate({'opacity': '1'},500);

			$(".newsletter").animate({'padding-bottom':'68px'},500);
			$("label[for=es_txt_name]").animate({'opacity': '1'},500);
			$("label[for=es_txt_name]").animate({'margin-top': '0px'},500);
			
			$(".newsletter input[type=button]").animate({'margin-top':'3px!important'},700);
		});
	});	

	$(document).ready(function() {
	$('#slider').cycle({ 
		fx:      'fade', 
		fx:   'scrollHorz', 
		next:	'#next',
		prev:	'#prev'
		 
	});
});
})();